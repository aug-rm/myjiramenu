package de.aug_rm.myjiramenu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.*;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import static com.google.common.base.Preconditions.*;

import java.io.IOException;
import java.net.URI;
import java.util.*;

public class AdminServlet extends HttpServlet{
	private static final Logger log = LoggerFactory.getLogger(AdminServlet.class);
	private final UserManager userManager;
	private final LoginUriProvider loginUriProvider;
    private final ActiveObjects ao;
    private final MenuService myMenuService;
    private static final String TEMPLATE_PATH = "/templates/admin.vm";
    private final TemplateRenderer renderer;
    private final I18nResolver i18n;
     
	public AdminServlet(
			UserManager userManager,
			LoginUriProvider loginUriProvider,
			ActiveObjects ao,
            MenuService service,
			TemplateRenderer renderer,
            I18nResolver i18n)
	{
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.ao = ao;
		this.myMenuService = service;
		this.renderer = renderer;
		this.i18n = checkNotNull(i18n);
	}
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		UserProfile user = userManager.getRemoteUser(request);
		if (user == null || !userManager.isAdmin(user.getUserKey()))
		{
			redirectToLogin(request, response);
			return;
		}

        final MenuEntity menuEntity = myMenuService.getMenuEntity();
        final List<MenuListItem> menuItemList = Lists.newArrayList(); 
        final Map<String, Object> context = Maps.newHashMap();
		
        for (MenuListItem item : myMenuService.allListItems())
        	menuItemList.add(item);

        if (menuItemList.isEmpty())
        	menuItemList.add(myMenuService.addListItem(i18n.getText("my-jira-menu-admin-menu-item.new"), ""));
        
        context.put("entity", menuEntity);
        context.put("list", menuItemList);
        
		response.setContentType("text/html;charset=utf-8");
		renderer.render(TEMPLATE_PATH, context, response.getWriter());
    }
 
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response)
        throws ServletException, IOException
    {
    	final String title = req.getParameter("entity.title");
    	final String[] labels = req.getParameterValues("list.label");
    	final String[] links = req.getParameterValues("list.link");
    	
    	final Iterator<MenuListItem> itemIterator = myMenuService.allListItems().iterator();
    	
    	myMenuService.getMenuEntity().setTitle(title);
        for( int i=0; i<labels.length && i<links.length; i++)
        {
        	if (itemIterator.hasNext())
        	{
        		MenuListItem item = itemIterator.next();
        		item.setLabel(labels[i]);
        		item.setLink(links[i]);
        		item.save();
        	}
        	else
        		myMenuService.addListItem(labels[i], links[i]);
        }
        
        while (itemIterator.hasNext())
        	myMenuService.deleteListItem(itemIterator.next());

        response.sendRedirect(req.getContextPath() + "/plugins/servlet/myjiramenu/adminservlet");
    }

	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	}

	private URI getUri(HttpServletRequest request)
	{
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null)
		{
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}
}