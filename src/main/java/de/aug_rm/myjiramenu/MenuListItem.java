package de.aug_rm.myjiramenu;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface MenuListItem extends Entity {
    
	public String getLabel();
	public void setLabel(String label);

	public String getLink();
	public void setLink(String link);	
	
	public MenuEntity getMenuEntity();
	public void setMenuEntity(MenuEntity menuEntity);	
}