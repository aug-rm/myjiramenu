package de.aug_rm.myjiramenu;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.OneToMany;

@Preload
public interface MenuEntity extends Entity {

	public String getTitle();
	public void setTitle(String title);
	
	@OneToMany
	public MenuListItem[] getMenuItems();	
}