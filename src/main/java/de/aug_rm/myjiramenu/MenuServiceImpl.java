package de.aug_rm.myjiramenu;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.message.I18nResolver;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import java.sql.SQLException;

public class MenuServiceImpl implements MenuService
{
    private final ActiveObjects ao;
    private final I18nResolver i18n;    
 
    public MenuServiceImpl(I18nResolver i18n, ActiveObjects ao)
    {
        this.i18n = checkNotNull(i18n);
        this.ao = checkNotNull(ao);
    }
 
    @Override
    public MenuEntity getMenuEntity() {
        try
        {
            final MenuEntity menuEntity = getSingleMenuEntity();
            return menuEntity != null ? menuEntity : createMenuEntity(i18n.getText("my-jira-menu-admin-menu-title.new"));
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public MenuListItem addListItem(String label, String link)
    {
        final MenuListItem item = ao.create(MenuListItem.class);
        item.setMenuEntity(getMenuEntity());
        item.setLabel(label);
        item.setLink(link);
        item.save();
        return item;
    }
  
	@Override
    public MenuListItem getListItem(int id) {
	    return ao.get(MenuListItem.class, id);
    }

    @Override
    public Iterable<MenuListItem> allListItems() {
        return newArrayList(ao.find(MenuListItem.class));
    }

	@Override
    public void deleteListItem(MenuListItem listItem) {
        ao.delete(listItem);
    }
	
    private MenuEntity getSingleMenuEntity() throws SQLException
    {
        final MenuEntity[] menuEntities = ao.find(MenuEntity.class);

        if (menuEntities.length > 1)
            throw new IllegalStateException("Application cannot have more than 1 menu");

        return menuEntities.length > 0 ? menuEntities[0] : null;
    }

    private MenuEntity createMenuEntity(final String title) throws SQLException
    {
        final MenuEntity menuEntity = ao.create(MenuEntity.class);
        menuEntity.setTitle(title);
        menuEntity.save();
        return menuEntity;
    }	
}
