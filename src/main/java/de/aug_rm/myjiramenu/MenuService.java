package de.aug_rm.myjiramenu;

import com.atlassian.activeobjects.tx.Transactional;
 
@Transactional
public interface MenuService
{
	/**
     * Gets the only menu of the application. Creates one if it doesn't already exists.
     *
     * @return the menu
     */
	public MenuEntity getMenuEntity();

    public MenuListItem getListItem(int id);
    public Iterable<MenuListItem> allListItems();

    public MenuListItem addListItem(String label, String link);
    public void deleteListItem(MenuListItem listItem);
}
